# Test files

The contents of these folders were obtained from public-facing websites offering DGN drawings and other files for download.

They are being used purely as test input for identifying correct/incorrect format detection, including false positives.

If you think any of these files should not be here, let me know.
